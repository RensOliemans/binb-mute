# 1.3
#### Fixed
Now also mutes messages that were sent via DMs

# 1.2.1
#### Fixed
* Only remove messages after someone has been muted
#### Changed
* Other icon, slightly different color

## 1.1.1
Internal code improvements

# 1.1
#### Added
* Muted icon wiggles when a muted person speaks
* Pointer cursor when hovering over the icon

# 1.0
First production-ready version.