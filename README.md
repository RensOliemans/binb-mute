# Binb mute

This is a WebExtension (currently only for Firefox) which adds mute-functionality to [binb](https://binb.co/),
an online song-guessing game.

Find it here: https://addons.mozilla.org/addon/binb-mute/  
Or download the project as a .zip file ([here](https://gitlab.com/RensOliemans/binb-mute/-/archive/main/binb-mute-main.zip))
and install it locally as a temporary add-on.
