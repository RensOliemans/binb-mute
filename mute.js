const usersId = "users";
const mutedValue = "true";
const chatId = "chat";
const nameClassName = "name";
const mutedIcon = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Antu_player-volume-muted.svg/512px-Antu_player-volume-muted.svg.png";
const volumeIcon = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Antu_player-volume.svg/512px-Antu_player-volume.svg.png";


const addButtons = () => {
  const users = document.getElementById(usersId);
  users.childNodes.forEach(setIcon);
};

const setIcon = (node) => {
  const name = node.getElementsByClassName(nameClassName)[0].innerText;
  const muted = window.localStorage.getItem(name) === mutedValue;

  const id = `muted-${name}`;
  const element = document.getElementById(id);
  if (element)
    element.remove();

  const icon = muted ? mutedIcon : volumeIcon;
  const button = document.createElement("img");
  button.setAttribute("src", icon);
  button.setAttribute("id", id);
  button.classList.add("mute");

  const onSuccess = () => setIcon(node);
  button.addEventListener("click", () => mute(name, onSuccess));

  node.insertBefore(button, node.childNodes[0]);
};


const mute = (name, onSuccess) => {
  const ls = window.localStorage;
  const status = ls.getItem(name);
  if (!status)
    ls.setItem(name, "true");
  else
    ls.removeItem(name);

  onSuccess();
};

const handleMute = () => {
  const chat = document.getElementById(chatId);
  
  const observer = new MutationObserver(removeMutedChats);
  const config = { childList: true };
  
  observer.observe(chat, config);
};

const removeMutedChats = (mutations) => {
  const chat = document.getElementById(chatId);

  mutations[0].addedNodes.forEach(li => {
    const message = li.innerText;
    const parts = message.split(':');
    const sender = message[0] === '(' ? parts[0].slice(6, -1) : parts[0];
    const muted = window.localStorage.getItem(sender) === mutedValue;
    if (muted) {
      console.log(`Removing message: "${message}"`)
      chat.removeChild(li);
      wiggleIcon(sender);
    }
  });
};

const wiggleIcon = (sender) => {
  const id = `muted-${sender}`;
  const icon = document.getElementById(id);
  icon.classList.add("wiggle");

  const wiggleDuration = 820;
  setTimeout(() => icon.classList.remove("wiggle"), wiggleDuration)
};


const handleUsersChange = () => {
  const users = document.getElementById(usersId);

  const observer = new MutationObserver(mutatedUsers);
  const config = { childList: true };

  observer.observe(users, config);
};


const mutatedUsers = (mutationRecords) => {
  if (mutationRecords[0].target.id === usersId) {
    addButtons();
  }
};

addButtons();
handleMute();
handleUsersChange();
